#!/usr/bin/python -u

import sys
import os
import errno
import logging
from argparse import ArgumentParser
from multiprocessing import Process
import config

gatewayName=''
serviceName=''
BASEDIR=''

EXECUTION_LOG_PATH = '/tmp/log-kickstart.log'
LOG_TEXT_FORMAT = '%(asctime)s %(levelname)s:%(message)s'
MAVEN_COMMAND = './mvnw package -Pprod doeckerfile:build'
JHIPSTER_COMMAND = 'jhipster'
JHIPSTER_REGD_COMMAND = 'java -jar ./jhipster-registry-4.0.0.war &'
ouputRedirection = ' >> '+ EXECUTION_LOG_PATH + ' 2>&1'
#JDL_FILEPATH = BASEDIR + os.sep + 'bank-jdl-jhipster.jh'

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def printLogData(logMessage, logLevel=1 ):
    print ('- ' + logMessage + '\n' )
    #if not logLevel:
    #    logLevel=1
    if ( logLevel == 1):
        logging.info(logMessage)
    elif ( logLevel == 2):
        logging.debug(logMessage)
    else :
        logging.error(logMessage)
 

def takePause(timeSec):
    os.system ('sleep ' + str(timeSec))

def doCommit (appName):
    printLogData ("Commiting generated application, " + appName + " to Gitlab project, " + appName, 1)
    # checkin
    gitUrl = "https://gitlab.com/" + gitlabUser + "/" + appName + ".git"
    os.system('git remote add origin ' + gitUrl + ouputRedirection)
    #os.system('git pull origin master' + ouputRedirection)
    os.system('git add .' + ouputRedirection)
    os.system('git commit -m "Committing Digital Workbench generated code for ' + appName + '"' + ouputRedirection)
    os.system('git push -u origin master -f' + ouputRedirection)

# Create badic folder structure
def createPrerequisiteStruct ():
    os.chdir (config.workspace_location)
    printLogData ("Creating Application folder structure under: " + BASEDIR, 1 )
    mkdir_p(BASEDIR)
    #os.system("cp ./jhipster-registry-4.0.0.war " + BASEDIR + os.sep)
    os.system("cp ./*.json " + BASEDIR + os.sep)
    os.system("cp ./*.js " + BASEDIR + os.sep)
    os.system("cp ./*.jh " + BASEDIR + os.sep)
    os.system("cp ./*.yml " + BASEDIR + os.sep)
    os.system("cp ./*.sh " + BASEDIR + os.sep)
    os.system("cp ./*ocker* " + BASEDIR + os.sep)
    os.system("cp ./*.ico " + BASEDIR + os.sep)
    os.system("cp ./*.png " + BASEDIR + os.sep)
    os.system("cp ./*.html " + BASEDIR + os.sep)
    os.system("cp -r ./k8files " + BASEDIR + os.sep)

def configureGateway ():
    printLogData ("Configuring the Gateway Application Structure. This operation may take sometime...", 1)
    mkdir_p (GATEWAYPATH)
    os.chdir(GATEWAYPATH)
    os.system ("sed -e 's/<GATEWAYSERVICE>/" + gatewayName + "/g' " + BASEDIR + "/yo-gw.json > ./.yo-rc.json")
    os.system (JHIPSTER_COMMAND  + ouputRedirection)

    os.system("cp ../home.component.html ./src/main/webapp/app/home/home.component.html")
    #os.system("cp ../gateway-footer.component.html ./src/main/webapp/app/layouts/footer/footer.component.html")
    ##os.system("cp ../gateway-navbar.component.html ./src/main/webapp/app/layouts/navbar/navbar.component.html")
    os.system("cp ../gateway-favicon.ico ./src/main/webapp/favicon.ico")
    os.system("cp ../brand-small.png ./src/main/webapp/content/images/hipster.png")
    os.system("cp ../brand-small.png ./src/main/webapp/content/images/logo-jhipster.png")
    os.system("cp ../brand-small.png ./src/main/webapp/content/images/hipster512.png")
    os.system("cp ../brand-small.png ./src/main/webapp/content/images/hipster256.png")
    os.system("cp ../brand-small.png ./src/main/webapp/content/images/hipster2x.png")
    os.system ('yarn build '  + ouputRedirection)

    # manually modifying the jest conf to avoid the jhipster issue with localstorage
    os.system ('cp -f ../gateway-jest.conf.js ./src/test/javascript/jest.conf.js')

    docker_reg_path="registry.hub.docker.com/" + config.docker_username + '/' + gatewayName

    # setup pipeline
    os.system("cp ../gateway-gitlab-ci.yml  ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's#<DOCKER_REG_PATH>#" + docker_reg_path + "#g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<DOCKER_USERNAME>/" + config.docker_username + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<DOCKER_PASSWORD>/" + config.docker_password + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_CLUSTER_ZONE>/" + config.gcloud_clusterzone + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_PROJECT_NAME>/" + config.gcloud_projectname + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_CLUSTER_NAME>/" + config.gcloud_clustername + "/g' ./.gitlab-ci.yml")

    # Kubernetes configuration, manually done, as the jhipster command needs user input which canot be provided through bot commands
    mkdir_p ('./src/main/k8/')
    os.system ('cp -r ../k8files/service-k8/* ./src/main/k8/')
    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + gatewayName + "/g' ./src/main/k8/kubectl-apply.sh")

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + gatewayName + "/g' ./src/main/k8/kubectl-apply.sh")
    os.system ('chmod +x ./src/main/k8/kubectl-apply.sh')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + gatewayName + "/g' ./src/main/k8/service/service-deployment.yml")
    os.system ('mv -f ./src/main/k8/service/service-deployment.yml ./src/main/k8/service/' + gatewayName + '-deployment.yml')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + gatewayName + "/g' ./src/main/k8/service/service-mysql.yml")
    os.system ('mv -f ./src/main/k8/service/service-mysql.yml ./src/main/k8/service/' + gatewayName + '-mysql.yml')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + gatewayName + "/g' ./src/main/k8/service/service-service.yml")
    os.system ('mv -f ./src/main/k8/service/service-service.yml ./src/main/k8/service/' + serviceName + '-service.yml')

    os.system ('rm -f ./src/main/k8/service/*.bak')
    os.system ('rm -f ./src/main/k8/*.bak')
    os.system ('mv -f ./src/main/k8/service ./src/main/k8/' + gatewayName)

    # setup dockerfile
    # os.system ("sed -e 's/<SERVICEINSTANCE>/" + serviceName + "/g' " + BASEDIR + "/service-dockerfile > ./Dockerfile")
    os.system("cp ../gateway-dockerfile  ./Dockerfile")
    os.system("cp ../service-entrypoint.sh  ./entrypoint.sh")
    os.system("chmod +x  ./entrypoint.sh")

    # setup k8 cluster configuration
    #os.system ("sed -e 's/<GCPROJECTNAME>/" + serviceName + "/g' " + BASEDIR + "/k8-configuation > ./src/main/k8/k8-configuation")
    os.system ('cp -f ../google-key.json ./key.json')

    importJdlEntities (GATEWAYPATH)

    # checkin
    doCommit(gatewayName)

    os.chdir (BASEDIR)

def configureServiceInstance ():
    printLogData ("Configuring the Service Application Instance Structure. This operation may take sometime...", 1)
    mkdir_p (SERVICEPATH)
    os.chdir(SERVICEPATH)
    #os.system("cp ../yo-serv.json  ./.yo-rc.json")
    os.system ("sed -e 's/<SERVICEINSTANCE>/" + serviceName + "/g' " + BASEDIR + "/yo-serv.json > ./.yo-rc.json")
    os.system (JHIPSTER_COMMAND + ouputRedirection)

    docker_reg_path="registry.hub.docker.com/" + config.docker_username + '/' + serviceName

    # setup pipeline
    os.system("cp ../service-gitlab-ci.yml  ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's#<DOCKER_REG_PATH>#" + docker_reg_path + "#g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's#<DOCKER_USERNAME>#" + config.docker_username + "#g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's#<DOCKER_PASSWORD>#" + config.docker_password + "#g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_CLUSTER_ZONE>/" + config.gcloud_clusterzone + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_PROJECT_NAME>/" + config.gcloud_projectname + "/g' ./.gitlab-ci.yml")
    os.system ("sed -i.bak 's/<GC_CLUSTER_NAME>/" + config.gcloud_clustername + "/g' ./.gitlab-ci.yml")

    # Kubernetes configuration, manually done, as the jhipster command needs user input which canot be provided through bot commands
    mkdir_p ('./src/main/k8/')
    os.system ('cp -r ../k8files/service-k8/* ./src/main/k8/')
    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + serviceName + "/g' ./src/main/k8/kubectl-apply.sh")

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + serviceName + "/g' ./src/main/k8/kubectl-apply.sh")
    os.system ('chmod +x ./src/main/k8/kubectl-apply.sh')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + serviceName + "/g' ./src/main/k8/service/service-deployment.yml")
    os.system ('mv -f ./src/main/k8/service/service-deployment.yml ./src/main/k8/service/' + serviceName + '-deployment.yml')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + serviceName + "/g' ./src/main/k8/service/service-mysql.yml")
    os.system ('mv -f ./src/main/k8/service/service-mysql.yml ./src/main/k8/service/' + serviceName + '-mysql.yml')

    os.system ("sed -i.bak 's/<SERVICEINSTANCE>/" + serviceName + "/g' ./src/main/k8/service/service-service.yml")
    os.system ('mv -f ./src/main/k8/service/service-service.yml ./src/main/k8/service/' + serviceName + '-service.yml')

    os.system ('rm -f ./src/main/k8/service/*.bak')
    os.system ('rm -f ./src/main/k8/*.bak')
    os.system ('mv -f ./src/main/k8/service ./src/main/k8/' + serviceName)

    # setup dockerfile
    # os.system ("sed -e 's/<SERVICEINSTANCE>/" + serviceName + "/g' " + BASEDIR + "/service-dockerfile > ./Dockerfile")
    os.system("cp ../service-dockerfile  ./Dockerfile")
    os.system("cp ../service-entrypoint.sh  ./entrypoint.sh")
    os.system("chmod +x  ./entrypoint.sh")

    # setup k8 cluster configuration
    os.system ('cp -f ../google-key.json ./key.json')

    importJdlEntities (SERVICEPATH)

    # checkin
    doCommit(serviceName)

    os.chdir (BASEDIR)

def startJhipsterRegistry ():
    printLogData ("Started Jhipster Registry in the background from location: " + BASEDIR, 1)
    os.chdir (BASEDIR)
    os.system( JHIPSTER_REGD_COMMAND + ouputRedirection)
    takePause(20)

def startMavenCompilation (dirName):
    printLogData ("Started parallel Maven Compilation for " + dirName, 1)
    os.chdir(dirName)
    os.system(MAVEN_COMMAND + ouputRedirection)
    os.chdir (BASEDIR)
    takePause(60)

def importJdlEntities (dirName) :
    printLogData ("Importing the defined entities in " + dirName, 1)
    os.chdir (dirName)
    os.system (JDL_IMPORT_COMMAND + ouputRedirection) 

def runInParallel(*fns):
  proc = []
  for fn in fns:
    p = Process(target=fn)
    p.start()
    proc.append(p)
  for p in proc:
    p.join()

def createDockerImages (imageName):
    printLogData("Creating Docker Image for: " + imageName, 1)
    os.system("docker image tag " + imageName + " " + config.docker_username + "/" + imageName)
    os.system("docker push " + config.docker_username + "/" + imageName)

def deployToGoogleCloud (imageName):
    printLogData("About to deploy Image for: " + imageName, 1)
    os.chdir(BASEDIR)
    os.system("kubectl apply -f " + imageName)

def printExternalUrlInformation ():
    printLogData("Services are deployed to Production. About to generate External Access URL")

########
# MAIN #
########
logging.basicConfig(filename=EXECUTION_LOG_PATH, format=LOG_TEXT_FORMAT, level=logging.DEBUG)
parser = ArgumentParser()
parser.add_argument("-o", "--operation",
                    help="Operation to Perform")
parser.add_argument("-b", "--basedir",
                    help="Application Base Dir")
parser.add_argument("-g", "--microserviceGateway",
                    help="Gateway Application Base Name")
parser.add_argument("-s", "--microserviceApplication",
                    help="Microservice Application Service Name")
parser.add_argument("-j", "--jdlfilepath",
                    help="Application Entity Definition JDL filepath")
#parser.add_argument("-u", "--gitlabuser",
                    #help="Gitlab Username")
#parser.add_argument("-p", "--gitlabprojectname",
                    #help="Gitlab Project")

arguments = parser.parse_args()


#printLogData ("###### Started Execution ###### ", 2)

operationName=arguments.operation
BASEDIR=arguments.basedir
gatewayName=arguments.microserviceGateway
serviceName=arguments.microserviceApplication
gitlabUser=config.gitlab_username
#gitlabProjectname=arguments.gitlabprojectname

#printLogData ("Operation: " + str(operationName) + ", Gateway: " + str(gatewayName) + ", Service: " + str(serviceName) + ", BASEDIR: " + str(BASEDIR) + ", Git User: " + gitlabUser, 2)
#gitUrl = "https://gitlab.com/" + gitlabUser + "/" + gitlabProjectname + ".git"

GATEWAYPATH = BASEDIR + os.sep + gatewayName
SERVICEPATH = BASEDIR + os.sep + serviceName

JDL_FILEPATH=arguments.jdlfilepath
JDL_IMPORT_COMMAND = 'jhipster import-jdl ' + JDL_FILEPATH + ' --force'

#printLogData ("Operatrion: " + operationName + ", Gateway: " + gatewayName + ", Service: " + serviceName + ", BASEDIR: " + BASEDIR, 2)


#if os.path.exists(BASEDIR):
#    printLogData ("Base Directory, " + BASEDIR + " already present, skipping folder application configuration", 1)
if operationName == 'BootstrapBase':
    createPrerequisiteStruct ()

    # startJhipsterRegistry ()
elif operationName == 'BootstrapGateway':

    configureGateway ()
elif operationName == 'BootstrapService':
    configureServiceInstance ()

#elif operationName == 'ImportJdl':
#    JDL_FILEPATH=arguments.jdlfilepath
#    JDL_IMPORT_COMMAND = 'jhipster import-jdl ' + JDL_FILEPATH + ' --force'

#    importJdlEntities (GATEWAYPATH)
#    importJdlEntities (SERVICEPATH)
else:
    printLogData ("Invalid Option : " + operationName)


    printLogData ("Do Nothing... : " + operationName)
    #startMavenCompilation (GATEWAYPATH)
    #startMavenCompilation (SERVICEPATH)

    #createDockerImages (gatewayName)
    #createDockerImages (serviceName)


    #deployToGoogleCloud ('registry')
    #deployToGoogleCloud (gatewayName)
    #deployToGoogleCloud (serviceName)
    #takePause (10)

    #printExternalUrlInformation ()


#printLogData ("###### Completed Execution ###### ", 2)
